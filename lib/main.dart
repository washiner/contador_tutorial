import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MeuApp());
}

class MeuApp extends StatelessWidget {
  const MeuApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Meu app contador",
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int numInicial = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            setState(() {
              numInicial = numInicial - 2;
            });
          },
          icon: Icon(Icons.g_translate_sharp),
        ),
        title: Text("Contador APP"),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                numInicial = numInicial + 3;
              });
            },
            icon: Icon(Icons.headphones),
          ),
        ],
      ),
      body: Center(
        child: Text(
          "$numInicial",
          style: TextStyle(fontSize: 72),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        //backgroundColor: Colors.black,
        onPressed: () {
          setState(() {
            numInicial = numInicial + 1;
          });
        },
      ),
    );
  }
}
